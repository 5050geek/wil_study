# LINUX SKILL BUILDING GAME

#### SKILLS TEAMS

- **TUX** 
- **BASH**
- **OS** 
- **ROOT**

#Machines
- **Machine 1 - TUX: 161.35.2.244**
- **Machine 2 - BASH : 161.35.56.147**
- **Machine 3 - OS: 143.244.161.38**
- **Machine 4 - ROOT: 147.182.208.49**

> Please join your assigned group. If you don't have a group, put your name and skill level in that chat.  

### WEEK 1
#### Week 1 Teams
- **Team TUX**
    - Lori, Fre, Ron
- **Team BASH**
    - Russ, Evelyn, Sim
- **Team OS**
    - Tunde, Rachel, Desire
- **Team ROOT**
    - Danny, Mya, Rico
- **Team Test**
   - new members

#### Week 1 | Game Commands

1. What do these directories do?
    - /etc, /proc, /var, /lib64, /boot
2. What is cron job?
    - What is the structure of a cronjob?
3. Configure a firewalld to open port 636
    - What is firewalld?
4. Find the ip address of your machine?
    - How to find the default gateway?
5. What is the kernel?
    - Find what version of the kernel is installed?
6. Turn off SELINUX across persistent reboots
    - Turn on SELINUX
7. Install rsync
    - Rsync /etc to /tmp

### WEEK 2
#### Week 2 Teams
- **Team TUX**
    -   Lori, Fre, Luis, Natasha,Millissa
- **Team BASH**
    - Russ, Evelyn, Glen, Sim,ELIE,Sasha
- **Team OS**
    - Tunde, Rachel, Desire,Darrell, Jdach ,Stacey, Felicia, Tunde
- **Team ROOT**
    - Danny, Mya, Rico,


#### Week 2 |  Game Commands

1. Setup users so they can ssh to the various boxes
    - Command options: rsync, scp, 
    - Create ssh keys for one of the users (pick one)
2. Create a file in the /tmp directory 
    - Add the following content  “Please make sure this works”
    - Now rsync the file over to /tmp to the new machine provided
3. Setup SUDOERS on machine 1 
    - Let all the users sudo without using a password
4. Install httpd package
    - Ensure that httpd is running
    - Start the service
    - Go to the public ip and check the results
    - Verify its working across reboots
    - To ensure it works,  reboot the machine


### WEEK 3   
#### Week 3 Teams
- **Team TUX**
    -   Lori, Fre, Luis, Natasha,Millissa
- **Team BASH**
    - Russ, Evelyn, Glen, Sim,ELIE,Sasha
- **Team OS**
    - Tunde, Rachel, Desire,Darrell, Jdach ,Stacey, Felicia, Tunde
- **Team ROOT**
    - Danny, Mya, Rico

> This week communication is key.

#### Week 3 | Game Commands 

- **Team 1 - BASH** 
    - Pick a Project Manager
    - Create `ssh` keys with a particular user
    - Install `git`
    - Clone this repository `https://gitlab.com/5050geek/wil_study.git`
    - `mkdir` called `terraform`
        - `cd` the `terraform directory`
        - install terraform: follow these instructions https://learn.hashicorp.com/tutorials/terraform/install-cli
    - Build machines today for all the teams

- **Team 2 - OS**
    - Pick a Project Manager
    - Create a presentation using Google Slides
    - What is Agile Methodology?
    - What is Kaban?
    - Define what sprints are?
    - What is the backlog?
    - What are issues?
    - What is an Epic?


- **Team 3 - TUX** 
    - Pick a Project Manager  
    - Communicate back with **Team 1**, find out what users they used to create keys for
    - Create the user on your machine
    - Get an `http` server going on your box
    - Setup `ssl certs` on the box (self signed)
    - In your `index.html` file. Be creative and type something in there
    - Open up the `firewall` for this machine
    - Use `curl` or `wget`
    - 'Html https://www.w3schools.com/html/html_styles.asp`


- **Team 4 (Security Team)**
    - Install `wireshark`
    - Learn how to use `tcpdump`
    - Get an ip from Team 1 and scan for open ports
    - Look up the `cves` for operating system running on your box
    - Use the `cves` list to find highs lows and critical
    - Scan all machines on that Team 1 created
    - Post your findings in the Jira ticket



### WEEK 4 
#### Week 4 Teams
- **Team TUX**
    -   Lori, Fre, Luis, Natasha,Millissa
- **Team BASH**
    - Russ, Evelyn, Glen,ILmiya, 
- **Team OS**
    -  Desire,Stacey,Charlena 
- **Team ROOT**
    - Joe, Rico

- **Team 5 - ROOT- (Test Team)**
    - How do you test terraform?
    - How do you test webpages?
    - What do you use to test ansible?
    - How to test tar files

- **Team 1 - BASH- (Ansible Team)
   - Install ansible
   - Set ansible up to communicate locally to the box you are on
   - Use ansible to create users (all the people on the meetup tonight)
   - Use ansible to set the user passwd to expire in 90days vs 60 days
   - Set 4 of the users to have a shell login of ksh
   - Remember this is all in the book and online

- **Team 2 - OS -  ( Tar team)
    - create a tar file of your /etc/ directory
      - when using tar ensure that use the date in tar command (use man pages)
    - rsync that tar file to team 3 /tmp folder
    - assign a md5sum to the file ( if possible)
  
- **Team 3 - TUX - (Virtual Host)
    - Install cockpit on the machines
    - Be ready to present what cockpit does
    - Be ready to present and click through cockpit screens
    - You should be able to give stats of the machine

- **Team 4
    - Give a tour of redhat customer portal
    - You will have to register your name and so forth
    - Use redhat developer subscription
    - what is the latest OS 
    - What cves are out there for the various oses. ( rhel 8, 9)
    - How do you just install security updates ( there is a command)

- ** Nov 2 (https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9)
     - Review Product documentation from Redhat
       - Basic System Settings
       - DNF Tool
       - Monitoring and Updating the kernel
       - Monitoring and Managing Performance
       - System Hardening
       - Managing and Monitoring Security Updates
       - Selinux
       - Configuring Firewalls and Packet Filters
       - Configuring and Managing Networking
       - Managing Filesystems
       - Configuring Networking Filesystems
       - Redhat in the Public Cloud
       - Quay

- ** Nov 9
     - LVM Resize (https://www.redhat.com/sysadmin/resize-lvm-simple)
     - File Permissions (https://rol.redhat.com/rol/app/courses/rh124-9.0/pages/ch07)
     - Yum/Dnf (https://rol.redhat.com/rol/app/courses/rh124-9.0/pages/ch14)
     - Containers (https://rol.redhat.com/rol/app/courses/rh134-9.0/pages/ch11)
       - https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/building_running_and_managing_containers/index
     - Packer (https://cloud.google.com/build/docs/building/build-vm-images-with-packer)
     - Thick vs Thin (https://www.nakivo.com/blog/thick-and-thin-provisioning-difference/)
     - Understanding Pipelines (https://www.bmc.com/blogs/devops-container-pipeline/)

